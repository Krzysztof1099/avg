package com.demo.springboot.rest;

import com.demo.springboot.dto.DigitsDto;
import com.demo.springboot.service.Average;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/math")
public class DocumentApiController {

    private final Average average;
    @Autowired
    public DocumentApiController(Average average) {
        this.average = average;
    }

    @GetMapping("average")
    public ResponseEntity<DigitsDto> params(@RequestParam() float[] digits){
        return new ResponseEntity<>(average.Calculate(digits), HttpStatus.OK);
    }
}
