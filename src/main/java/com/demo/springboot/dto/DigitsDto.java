package com.demo.springboot.dto;

public class DigitsDto {
    float avg;

    public float getAverage() {
        return avg;
    }

    public DigitsDto(float avg) {
        this.avg = avg;
    }

}
